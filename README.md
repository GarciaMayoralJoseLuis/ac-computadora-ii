# Ac Computadora Ii

## Computadoras Electrónicas
#### Sintaxis Markdown
```plantuml
@startmindmap
*[#FFBBCC] Computadoras Electrónicas
	* Inicios del siglo XX: \nAparición y popularidad de las computadoras de propósito específico \n(funcionan par una sola cosa) en empresas y el gobierno.
	* 1904: 
		*[#lightgreen] En este año un físico Ingles llamado Jhon Ambrose Fleming inventó un componente térmico \nllamado **Válvula Termoiónica**. Años después serían usados como remplazos del Relé.
			*[#FFBBCC] componentes que la formaban
				*[#Orange] Un filamento.
				*[#Orange] Dos electrodos, un ánodo y un cátodo.
				*[#Orange] Todos ellos sellos sellados en un Bulbo de cristal.
			*[#FFBBCC] En 1906
				*[#Orange] Un científico americano llamado Lee De Forest, modificó la válvula termoiónica \nagregando un electrodo de control.
				*[#Orange] Funcionamiento
					*[#SkyBlue] Su funcionamiento era muy similar al Relé, ya que el electrodo de control \npermitía el flujo de electrones o lo impedia, permitiendo el cambio de estado.
				*[#Orange] Ventajas 
					*[#SkyBlue] *A diferencia del Relé, no tiene el problema de las partes móviles.
					*[#SkyBlue] *Cambiaban de estado miles de veces por segundo.
				*[#Orange] Desventajas
					*[#SkyBlue] *Fragiles \n*Costosos
				*[#Orange] Uso en aparatos electronicos de la epoca como:
					*[#SkyBlue] *Televisores \n*Radios \n*Tiempo después en las computadoras. 
	* 1940: 
		*[#lightgreen] *El precio de la **Válvula Termoiónica** se volvieron más accesibles para gobiernos, \nhaciendo posibles computadoras en base a este componente.
			*[#FFBBCC] Esto dio inicio a **la era de las computadoras electrónicas*.
	* 1941: 
		*[#lightgreen] En este año se dio la creación de la primera computadora que utilizó a gran escala a \nla **Válvula Termoiónica**, esta se llamó **Colossus Mark 1**.
	* 1943: 
		*[#lightgreen] El precio de la **Válvula Termoiónica** se volvieron más accesibles para gobiernos, \nhaciendo posibles computadoras en base a este componente.
			*[#FFBBCC] Creación y diseño
				*[#Orange] Fue diseñada por el ingeniero Tommy Flowers y es conocida \ncomo la primera computadora electrónica programable.
			*[#FFBBCC] Características
				*[#Orange] *Tenía 1600 tubos de vacío.
				*[#Orange] *La programación y configuraba de la computadora se realizaba por \nmedio de un tablero similar a los tableros de conmutación de los \nteléfonos de aquel entonces.
			*[#FFBBCC] Uso
				*[#Orange] Fue instalada en el parque Bletcheley en Reino Unido y fue utilizada para \ndecodificar las comunicaciones Nazi. Se utilizaron al menos 10 Colossus para \neste propósito.
	* 1944: 
		*[#lightgreen] Durante la segunda guerra mundial fue construida por IBM una de las \ncomputadoras electromecánicas más grandes del mundo, la **Hardvard Mar 1**
			*[#FFBBCC] Características de la Hardvard Mark 1
				*[#Orange] *765 mil componentes, entre ellos los **Relés**
				*[#Orange] *80 km de cable
				*[#Orange] *Un motor de 5 caballos de fuerza (HP)
				*[#Orange] *Un eje de 15 metros que atravesaba todo el dispositivo, con el fin de mantenerlo en sincronía.   
				*[#Orange] *tiempo por operación en la H Mark 1
					*[#SkyBlue] * 3 sumas/restas por segundo
					*[#SkyBlue] * Una multiplicación en 6 segundos
					*[#SkyBlue] * División en 15 segundos
					*[#SkyBlue] * Operaciones complejas en minutos
			*[#FFBBCC] Uso más sobresaliente
				*[#Orange] El proyecto Manhattan, proyecto el cual desarrolló la bomba atómica.
			*[#FFBBCC] Desventajas o problemas de la Hardvard Mark 1
				*[#Orange] *Los componentes mecánicos
					*[#SkyBlue] Este tipo de componentes son propensos al desgaste.
				*[#Orange] *Su tamaño
					*[#SkyBlue] debido a su tamaño atraía insectos.
						*[#GreenYellow] De aquí proviene el termino **BUG** para referirse a un problema o error \nen alguna computadora o durante la programación. Dicho termino fue \nacuñado en **1947** por la científica Grace Hopper.
	* 1946: 
		*[#lightgreen] En la Universidad de Pensilvania fue construida **ENIAC**, que sería la primer \n**computadora programable de programable de propósito general**, \ndiseñada por John Mauchly y j. Presper Eckert.
			*[#FFBBCC] Características
				*[#Orange] Su nombre proviene de las siglas, Calculadora, Integradora, Numerica, Electronica.
				*[#Orange] Podía realizar 5 mil sumas y restas de diez dígitos por segundo. Por encima de \ncualquier dispositivo de la fecha.
				*[#Orange] Se calcula que realizó más operaciones aritméticas que toda la humanidad hasta esa fecha.
				*[#Orange] Tenía la desventaja de estropearse a menudo por los tubos de vacío.
	* 1947: 
		*[#lightgreen] Los científicos del laboratorio BELL, Jonh Bardeen, Wiliam Shockley, \nWalter Brattain inventaron los **Transistores**
			*[#FFBBCC] Estructura de los transistores
				*[#Orange] Están hechos de Silicio, que en su estado puro es aislante pero que por medio de un proceso \nllamado dopaje se le agregar elementos como el aluminio o el arsénico, se les toda de propiedades \ncomo un exceso o defecto de electrones.
			*[#FFBBCC] Características y funcionamiento
				*[#Orange] Los primeros resistores podían realizar unos 10 mil cambios de estado por segundo (10000 Hertz).
				*[#Orange] El transistor se compone de 3 capas de este tipo de silicio en forma de sándwich.
					*[#SkyBlue] Cada capa representa un conecto que son
						*[#GreenYellow] Colector 
						*[#GreenYellow] Emisor
						*[#GreenYellow] Base
	* 1957: 
		*[#lightgreen] Gracias al avance y creación de los resistores se pudieron crear \ncomputadores más baratos y rápidos, como la **IBM 608**, la cual fue \nla primera computadora comercial basada en transistores.
			*[#FFBBCC] Podía realizar 4500 sumas y 80 divisiones o multiplicaciones por segundo
			*[#Orange] Esto permitió que las computadoras fueran más accesibles para oficinas y hogares. 
	* Hoy día: 
		*[#lightgreen] El avance en los transistores ha sido tal que su tamaño se ha reducido \nhasta los 50 nm cambiando de estado millones de veces y son de larga duración.
@endmindmap
```


## Arquitectura Von Neumann y Arquitectura Harvard
#### Sintaxis Markdown

```plantuml
@startmindmap
*[#PaleVioletRed] **Arquitectura Von Neumann y Arquitectura Harvard**
	* Ley de Moore
		*[#lightgreen] *Esta ley establece que la potencia o poder de procesamiento total de \nlas computadoras se duplica cada 12 meses. \n\n*Esta ley es proporcional al número de transistores en un procesador, \nes decir que, a mayor número de transistores, mayor es la velocidad \nde procesamiento.  
			*[#FFBBCC] Performance o rendimiento
				*[#BurlyWood] La velocidad del procesador se incrementa
				*[#BurlyWood] La capacidad de memoria se incrementa.
				*[#BurlyWood] La velocidad de la memoria corre siempre por \ndetrás de la velocidad del procesador.
			*[#FFBBCC] En la electronica
				*[#BurlyWood] El número de transistores por cada chip se duplica cada año
				*[#BurlyWood] Los costos por chip se mantienen sin cambios
				*[#BurlyWood] Cada 18 meses se duplica la potencia de cálculo sin afectar al costo.
	* Funcionamiento de una computadora
		*[#lightgreen] Antes los sistemas eran cableados
			*[#FFBBCC] Funcionamiento:
				*[#BurlyWood] La programación de estas computadoras era mediante el Hardware, es decir que, \ncuando necesitaban realizar otra tarea era necesario cambiar el hardware. 
				*[#BurlyWood] El procesamiento del sistema era el siguiente
					*[#SkyBlue] Datos
						*[#GreenYellow] Aquí se ingresan los datos 
					*[#SkyBlue] Secuencias de funciones 
						*[#GreenYellow] Estas se obtenían directamente del hardware, para realizar \notro tipo de operaciones era necesario cambiar el hardware.
					*[#SkyBlue] Resultados 
						*[#GreenYellow] Los datos obtenidos después de la secuencia de funciones
		*[#lightgreen] Ahora los sistemas son mediante el software
			*[#FFBBCC] Funcionamiento:
				*[#BurlyWood] La programación ahora es mediante el software, \nno es necesario realizar algún cambio de software.
				*[#BurlyWood] El procesamiento del sistema era el siguiente
					*[#SkyBlue] Datos
						*[#GreenYellow] Al igual que antes, se ingresan los datos de interés
					*[#SkyBlue] Secuencias de funciones
						*[#GreenYellow] A diferencia del antes, ahora se introduce un **intérprete de funciones** y \n**señales de control** gracias a los transistores, solo hay que definir por \nmedio de la programación lo que queremos.
					*[#SkyBlue] Resultados 		
	* Arquitectura Von Neumann
		*[#lightgreen] Arquitectura basada en la creada por el matemático \ny físico John Von Neuman en 1945.
			*[#FFBBCC] Características
				*[#BurlyWood] Los datos y programas se almacenan en una misma \nmemoria que sirve para lectura y escritura.
				*[#BurlyWood] El contenido de la memora es accesible por medio \nde la indicación de su posición sin importar el tipo.
				*[#BurlyWood] Ejecución secuencial
				*[#BurlyWood] Representación binaria
			*[#FFBBCC] Conforman la arquitectura de diseño \npara un computador digital (partes \nimportantes)
				*[#BurlyWood] Unidad de procesamiento o CPU
					*[#SkyBlue] Tiene
						*[#GreenYellow] Unidad de control
						*[#GreenYellow] Unidad aritmética lógica
						*[#GreenYellow] Registros
							*[#DodgerBlue] *Registro de instrucciones \n*Registro de direcciones de memoria \n*Registro de buffer de memoria \n*Registro de dirección E/S \n*Registro de buffer de E/S
				*[#BurlyWood] Memoria principal
					*[#SkyBlue] Puede almacenar instrucciones y datos
				*[#BurlyWood] Sistema de Entrada/Salida
			*[#FFBBCC] Modelo de Bus
				*[#BurlyWood] El bus es un dispositivo en común \nentre dos o más dispositivos y \npuede dividirse en:
					*[#BurlyWood] Bus del sistema
						*[#SkyBlue] Es el bus que comunica al procesador, memoria y E/S.
					*[#BurlyWood] El bus esta compuesto por
						*[#SkyBlue] Bus de control
						*[#SkyBlue] Bus de direcciones 
						*[#SkyBlue] Bus de datos e instrucciones
				*[#BurlyWood] El bus tiene como proposito reducir la cantidad de conexiones \nentre la CPU y sus sistemas
					*[#SkyBlue] Su uso tiene que ser controlado por \nmedio de un arbitraje para evitar la \nperdida de información
				*[#BurlyWood] Este modelos es una mejoria al modelo de Von-Neuman
			*[#FFBBCC] Funcionamiento según la \narquitectura Von-Neuman
				*[#BurlyWood] La función de una computadora es la ejecución de programas
					*[#SkyBlue] Los programas se localizan en la memoria y consisten en instrucciones
					*[#SkyBlue] Funcionamiento de la **CPU**
						*[#GreenYellow] Se encarga de ejecutar las instrucciones \na través de un clico de instrucción 
							*[#DodgerBlue] Dichas instrucciones consisten en secuencias de \n0 y 1 llamada código maquina
							*[#DodgerBlue] Las instrucciones se ejecutan por la CPU a grandes \nvelocidades
							*[#DodgerBlue] Un **CPU** de 3GHz realiza 3.000.000.000 de \noperaciones por segundo.
					*[#SkyBlue] Para la ejecución de instrucciones se usan diversos lenguajes
						*[#GreenYellow] *Bajo nivel
							*[#DodgerBlue] Lenguaje ensamblador
						*[#GreenYellow] *Alto nivel
							*[#DodgerBlue] Pyton
							*[#DodgerBlue] Java
							* Entre otros
			*[#FFBBCC] Ciclo de ejecución
				*[#BurlyWood] Fetch de instrucción (**FI**)
					*[#SkyBlue] Almacena la información de la siguiente instrucción
				*[#BurlyWood] Decode instrucción (**DI**)
					*[#SkyBlue] Decodifica la instrucción a un lenguaje entendible para la ALU
				*[#BurlyWood] Fetch de operandos (**FO**)
					*[#SkyBlue] Calcular operandos, obtiene de memoria los operandos para la instrucción
				*[#BurlyWood] Execute instrucción (**EI**)
				*[#BurlyWood] Write operand (**WO**)
			*[#FFBBCC] Ciclo de instrucción
				*[#BurlyWood] Partes que componen al ciclo
					*[#Red] **Inicio del ciclo**
					*[#SkyBlue] Captación de instrucción
					*[#SkyBlue] Decodificación de la operación de la instrucción
					*[#SkyBlue] Cálculo de la dirección del operando
						*[#GreenYellow] Varios operandos comparitdos con la siguiente parte
					*[#SkyBlue] Captación del operando
					*[#SkyBlue] Operación de datos
					*[#SkyBlue] Cálculo de la dirección del operando
					*[#SkyBlue] Almacenamiento del operando
						*[#GreenYellow] Varios operandos comparitdos con la siguiente parte
					*[#SkyBlue] Calculo de la dirección de la instrucción
					*[#Red] **Fin del ciclo**
	* Arquitectura Harvard
		*[#lightgreen] Se referia a las arquitecturas que usan \ndispositivos de almacenamiento separados
			*[#FFBBCC] Características 
				*[#BurlyWood] Utiliza un almacenamiento fisicamente seraparao, \nes decir, cuentan con dos memorias 
					*[#SkyBlue] Las instrucciones y los datos se almacenan \nen cachés separadas, mejora el rendimiento
					*[#SkyBlue] Esto soluciona el problema del cuello de \nbotella del modelo Von-Neuman
				*[#BurlyWood] Buses separados
					*[#SkyBlue] Esto optimiza la busqueda y el procesamiento de datos
						*[#GreenYellow] Se dividen en 
							*[#DodgerBlue] Bus de control
							*[#DodgerBlue] Bus de de instrucciones
							*[#DodgerBlue] Bus de datos
			*[#FFBBCC] Suele utilizarse en
				*[#BurlyWood] **PICs**
				*[#BurlyWood] **Microcontroladores**
				*[#BurlyWood] Generalmente utilizaods en productos de propósito específico
				*[#BurlyWood] Tambien utilizado en el procesamiento de audio, video, etc
			*[#FFBBCC] Conforman la arquitectura de diseño
				*[#BurlyWood] Unidad central de procesamiento (**CPU**)
					*[#SkyBlue] Tiene
						*[#GreenYellow] Unidad de Control (**UC**)
							*[#DodgerBlue] Lee la instrucción de la memoria de \ninstrucciones y genera señales de control
						*[#GreenYellow] Unidad Aritmética Lógica (**ALU**)
							*[#DodgerBlue] Ejecuta la instrucción dada por la UC \ny almacena el resultado en la memoria \nde datos
						*[#GreenYellow] Registros
				*[#BurlyWood] Memoria
					*[#GreenYellow] A la pequeña cantidad de memoria muy rapida \nque este a disposición del procesador se le llama \n**caché**
						*[#DodgerBlue] Las instrucciones y los datos se almacenan en \ncachés separadas para mejorar el rendimiento
						*[#DodgerBlue] Dada la separción fisica de la memoria tenemos
							*[#FloralWhite] Memoria de instrucciones
								*[#DodgerBlue] Almacena las instrucciones del programa que se deben \nejecutar
								*[#DodgerBlue] Se implementa utilizando memorias no volatiles como: 
									*[#GreenYellow] *ROM
									*[#GreenYellow] *PROM
									*[#GreenYellow] *EPROM
									*[#GreenYellow] *flash
							*[#FloralWhite] Memoria de Datos
								*[#DodgerBlue] Almacena los datos utilzados por los programas
								*[#DodgerBlue] Se implementa utilizando
									*[#GreenYellow] *RAM
									*[#GreenYellow] *SRAM
									*[#GreenYellow] *Incluso EEPROM o flash
				*[#BurlyWood] Sistema de Entrada/salida


@endmindmap
```








## Basura Electrónica
#### Sintaxis Markdown

```plantuml
@startmindmap
*[#PaleVioletRed] Basura Electrónica
	*[#OliveDrab] ¿Que es?
		*[#LightGreen] *Son todos los aparatos electronicos que agotaron su vida útil \n*También son conocidos como **RAEE**(Residuos de aparatos electronicos y electricos)
	* Pueden ser desechos como 
		*[#lightgreen] *Televisores \n*Celulares \n*Computadoras \n*etc...
	* Dichos desechos pueden ser reciclados
		*[#lightgreen] Proceso de reciclaje
			*[#SkyBlue] *Recolección
			*[#SkyBlue] *Selección según su categoría
			*[#SkyBlue] *Desarmado y clasificación
				*[#GreenYellow] En esta parte se recuperan todo tipo \nde metales y minerales
			*[#SkyBlue] *Reintroducción a una nueva \ncadena productiva (se reutiliza)
		*[#lightgreen] Al reciclar se extraen materiales, \ncomo lo son metales preciosos como 
			*[#SkyBlue] *Oro
			*[#SkyBlue] *Cobre
			*[#SkyBlue] *Plata
			*[#SkyBlue] *Cobalto
		*[#LightGreen] Reciclaje seguro
			*[#SkyBlue] Se realiza en plantas de reciclaje
			*[#SkyBlue] Es muy costoso reciclar electronicos de forma \nsegura, son muchas medidas para no generar \nun gran impacto al medio ambiente
			*[#SkyBlue] Es un proceso muy complicado
		*[#LightGreen] Reciclaje clandestino
			*[#SkyBlue] *Es ilegal
				*[#GreenYellow] No esta regularizado
				*[#GreenYellow] Se exporta de forma clandestina \nen grandes contenedores  a \npaises como
					*[#SkyBlue] *México
					*[#SkyBlue] *China
					*[#SkyBlue] *India
			*[#SkyBlue] *Usa métodos primitivos **(baratos y peligrosos)**
				*[#GreenYellow] Todos los residuos generados por \nestos metodos contaminan
			*[#SkyBlue] *Es perjudicial para la salud
				*[#GreenYellow] Debido a que los trabajadores \nse exponen a vapores tóxicos
		*[#lightgreen] Plantas de reciclaje
	* Malos usos
		*[#lightgreen] Gracias a un mal manejo de los desechos, \nmuchas veces pueden obtener información \npersonal por medio de discos duros o celulares
			*[#SkyBlue] Robo de identidad, falsificación \nson algunos malos usos
			*[#SkyBlue] Como contramedida, borrar todo tipo de información.
	* Contaminación
		*[#lightgreen] El 70% de las toxinas de los tiraderos \nproviene de los aparatos electrónicos
		*[#lightgreen] Las pilas botón contaminan hasta \n600 mil litros de agua
		*[#lightgreen] En Mexico se producen 300 mil toneladas \nde desechos electrónicos
		*[#lightgreen] Factores que generan contaminación
			*[#SkyBlue] Una creciente clase media
			*[#SkyBlue] El rapido avance tecnologico 
			*[#SkyBlue] Aparator electronicos diseñados para durar menos
				*[#GreenYellow] Su vida util es en promedio de 3 a 5 años
		*[#lightgreen] Contramedidas
			*[#SkyBlue] La Unión Europea lanzó una ley que por cada \n100 Toneladas de productos elecctronicos, \ncada país recolecte 45 Toneladas
			*[#SkyBlue] El reciclaje
				*[#SkyBlue] Ejemplo 
					*[#GreenYellow] Utilizar aluminio reciclado se emite menos CO2.
			*[#SkyBlue] Acudir a empresas de reciclaje
				*[#GreenYellow] Ejemplos
					*[#SkyBlue] En México: **TECHEMET** y **REMSA**
						*[#lightgreen] **REMSA** recicla el 90% de los desechos electrónicos
					*[#SkyBlue] En Costa Rica: **Servicios Ecológicos**
@endmindmap
```
